# WQQAmp

WQQAmp is a C++ code which computes the massive 1- and 2-loop amplitudes for the production of a W boson in association with a heavy-quark pair.

The code will be uploaded shortly to this repository.

## Support

For support, please contact the authors of the library:

Luca Buonocore [lbuono.at.physik.uzh.ch](mailto:lbuono@physik.uzh.ch)

Luca Rottoli  [luca.rottoli.theor.at.gmail.com](mailto:luca.rottoli.theor@gmail.com)

Chiara Savoini [csavo.at.physik.uzh.ch](mailto:csavo@physik.uzh.ch)

## Citation policy

If you use the code for a scientific publication you *must* cite

[Associated production of a W boson and massive bottom quarks at next-to-next-to-leading order in QCD]() L. Buonocore, S. Devoto, S. Kallweit, J. Mazzitelli, L. Rottoli and C. Savoini

[Pentagon functions for one-mass planar scattering amplitudes](https://inspirehep.net/literature/1947336), D. Chicherin, V. Sotnikov, S. Zoia: JHEP 01 (2022) 096

[Leading-color two-loop amplitudes for four partons and a W boson in QCD](https://inspirehep.net/literature/1944964), S. Abreu,  F. Febres Cordero, H. Ita, M. Klinkert, B. Page, V. Sotnikov: JHEP 04 (2022) 042

[The Singular behavior of massive QCD amplitudes](https://inspirehep.net/literature/734161) A. Mitov, S. Moch: JHEP 05 (2007) 001

## License

This work is licensed under GNU GPLv3.

